import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  [x: string]: any;
  Tasks = ['Story', 'Epic', 'Task']
  items = [
    { id: 1, name: 'Story' },
    { id: 2, name: 'Epic' },
    { id: 3, name: 'Task' },

  ];





  // isValidated = false;
  constructor(public fb: FormBuilder) { }
  myForm = this.fb.group({
    tasks: ['']
  })
  get tasks() {
    return this.myForm.get('tasks');
  }
  // changeTask(e:any) {
  //   console.log(e.value)
  //     this.this.Tasks.setValue(e.target.value, {
  //      onlySelf: true
  //    })
  // }
  submit() {

  }
  ngOnInit(): void {
  }

}
